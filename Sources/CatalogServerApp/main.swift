import PerfectHTTPServer

let httpServer = HTTPServer()
httpServer.serverPort = 8080
httpServer.addRoutes(setUpRouter());

do {
    try httpServer.start()
}
catch {
    print("Error connecting Server", error)
}
