// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CatalogServerApp",
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", from: "3.0.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .executableTarget(
            name: "CatalogServerApp",
            dependencies: [.product(name: "PerfectHTTPServer", package: "Perfect-HTTPServer")]),
        .testTarget(
            name: "CatalogServerAppTests",
            dependencies: ["CatalogServerApp"]),
    ]
)
