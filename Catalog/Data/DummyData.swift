//
//  DummyData.swift
//  CatalogAppServer
//
//  Created by Arpit Dixit on 15/01/22.
//

import Foundation

func getCatalogList() -> [String: Any] {
    return [
        "products": [
            ["id": 1,
             "name": "iPhone 12 Pro",
             "desc": "Apple iPhone 12th generation",
             "price": 999,
             "color": "#33505a",
             "image": "assets/images/iPhonePro.png"],
            ["id": 2,
             "name": "Pixel 5",
             "desc": "Google pixel phone 5th generation",
             "price": 699,
             "color": "#33505a",
             "image": "assets/images/Pixel5.png"],
            ["id": 3,
             "name": "M1 Macbook Air",
             "desc": "Apple macbook air with apple silicon",
             "price": 1099,
             "color": "#33505a",
             "image": "assets/images/macbookpro.png"],
            ["id": 4,
             "name": "Playstation 5",
             "desc": "Sony playstation 5th generation",
             "price": 500,
             "color": "#33505a",
             "image": "assets/images/ps5.png"],
            ["id": 5,
             "name": "Airpods Pro",
             "desc": "Apple airpods pro 5th generation",
             "price": 200,
             "color": "#33505a",
             "image": "assets/images/airpods.png"],
            ["id": 6,
             "name": "iPad Pro",
             "desc": "Apple iPad pro 2020 edition",
             "price": 799,
             "color": "#33505a",
             "image": "assets/images/ipad.png"],
            ["id": 6,
             "name": "iPad Pro",
             "desc": "Apple iPad pro 2020 edition",
             "price": 799,
             "color": "#33505a",
             "image": "assets/images/ipad.png"],
            ["id": 8,
             "name": "Galaxy S21",
             "desc": "Samsung galaxy s12 ultra 2020 edition",
             "price": 899,
             "color": "#33505a",
             "image": "assets/images/s21.png"],
        ]
    ]
}

func getCatalogDetailData() -> [[String: Any]] {
    return [
            ["id": 1,
             "name": "iPhone 12 Pro",
             "desc": "Apple iPhone 12th generation",
             "price": 999,
             "color": "#33505a",
             "image": "assets/images/iPhonePro.png"],
            ["id": 2,
             "name": "Pixel 5",
             "desc": "Google pixel phone 5th generation",
             "price": 699,
             "color": "#33505a",
             "image": "assets/images/Pixel5.png"],
            ["id": 3,
             "name": "M1 Macbook Air",
             "desc": "Apple macbook air with apple silicon",
             "price": 1099,
             "color": "#33505a",
             "image": "assets/images/macbookpro.png"],
            ["id": 4,
             "name": "Playstation 5",
             "desc": "Sony playstation 5th generation",
             "price": 500,
             "color": "#33505a",
             "image": "assets/images/ps5.png"],
            ["id": 5,
             "name": "Airpods Pro",
             "desc": "Apple airpods pro 5th generation",
             "price": 200,
             "color": "#33505a",
             "image": "assets/images/airpods.png"],
            ["id": 6,
             "name": "iPad Pro",
             "desc": "Apple iPad pro 2020 edition",
             "price": 799,
             "color": "#33505a",
             "image": "assets/images/ipad.png"],
            ["id": 6,
             "name": "iPad Pro",
             "desc": "Apple iPad pro 2020 edition",
             "price": 799,
             "color": "#33505a",
             "image": "assets/images/ipad.png"],
            ["id": 8,
             "name": "Galaxy S21",
             "desc": "Samsung galaxy s12 ultra 2020 edition",
             "price": 899,
             "color": "#33505a",
             "image": "assets/images/s21.png"],
    ]
}


func getIndexData() -> String {
    return """
<h1 style="color: #5e9ca0;">Server Side Programming in Swift</h1>
<h2 style="color: #2e6c80;">Swift can do a lot more than we think</h2>
<p>All the APIs that are being used by Whoosh app are written in Swift using the framework Perfect. Perfect has done all the heavy lifting, and all we need to do is use it's APIs.&nbsp;</p>
<p>In regards of NodeJS, Perfect is Express for us. In fact, a lot more. Here's what Perfect can do</p>
<h2 style="color: #2e6c80;">Some features of Perfect:</h2>
<ol style="list-style: none; font-size: 14px; line-height: 32px; font-weight: bold;">
<li style="clear: both;">Creating HTTP Server</li>
<li style="clear: both;">Routing</li>
<li style="clear: both;">DB Connections (Mongo, MySQL, Maria, Postgres etc)</li>
<li style="clear: both;">Web Sockets</li>
<li style="clear: both;">File Handling</li>
<li style="clear: both;">Server extensions for messaging queues (Kafka, Mosquitto, Zookeeper etc)</li>
<li style="clear: both;">Deployment (directly on EC2)</li>
<li style="clear: both;">... and a lot more</li>
</ol>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<h2 style="color: #2e6c80;">Let's give it a try:</h2>
<h2 style="color: #2e6c80; text-align: right;"><span style="color: #000000;">iCode</span></h2>
"""
}

