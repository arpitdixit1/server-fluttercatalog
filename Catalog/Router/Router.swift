//
//  Router.swift
//  CatalogAppServer
//
//  Created by Arpit Dixit on 15/01/22.
//

import Foundation
import PerfectHTTP

func setUpRouter() -> Routes {
    var routes = Routes()
    
    routes.add(method: .get, uri: "/") { request, response in
        response.setBody(string: getIndexData()).completed()
    }
    
    routes.add(method: .get, uri: "/catalog") { request, response in
        CatalogListController().handleCatalogRequest(request: request, response: response)
    }
    
    routes.add(method: .get, uri: "/catalog/{id}") { request, response in
        CatalogDetailController().handleCatalogDetailRequest(request: request, response: response)
    }
    
    return routes
}

