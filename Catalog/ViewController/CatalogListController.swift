//
//  CatalogListController.swift
//  CatalogAppServer
//
//  Created by Arpit Dixit on 15/01/22.
//

import Foundation
import PerfectHTTP

class CatalogListController {
    
    func handleCatalogRequest(request: HTTPRequest, response: HTTPResponse) {
        do {
           try response.setBody(json: getCatalogList())
                .setHeader(.contentType, value: "application/json")
                .completed(status: .ok)
        } catch {
            response.setBody(string: "Something went wrong").completed(status: .internalServerError)
        }
    }
}
