//
//  CatalogDetailController.swift
//  CatalogAppServer
//
//  Created by Arpit Dixit on 15/01/22.
//

import Foundation
import PerfectHTTP

class CatalogDetailController {
    
    func handleCatalogDetailRequest(request: HTTPRequest, response: HTTPResponse) {
        do {
            guard let catalogId = Int(request.urlVariables["id"] ?? "") else {
                response.setBody(string: "Catalog id is missing")
                    .completed(status: .badRequest)
                return
            }
            let catalogDetails = getCatalogDetailData().filter { item in
                if let id = item["id"] as? Int {
                    return id == catalogId
                }
                return false
            }
            try response.setBody(json: catalogDetails)
                .setHeader(.contentType, value: "application/json")
                .completed(status: .ok)
        } catch {
            response.setBody(string: "Something went wrong")
                .completed(status: .internalServerError)
        }
    }
}
